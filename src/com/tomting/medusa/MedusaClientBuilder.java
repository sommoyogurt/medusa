package com.tomting.medusa;

import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.tomting.orion.connection.Helpers;
import com.tomting.orion.connection.OCFI;


public class MedusaClientBuilder {
	private final static Logger LOGGER = Logger.getLogger(MedusaClientBuilder.class.getName());	
	
	private OCFI orionConnectionFactory;
	private String medusaTable;
	private Rubber rubber;
	
	public MedusaClientBuilder (String url, String table) {
		LOGGER.info(url);
		        
		orionConnectionFactory = com.tomting.aries.Helpers.getSharedACF(Helpers.getOCF(url), url);
        medusaTable = table;
        rubber = new Rubber (orionConnectionFactory, medusaTable, Constants.cleanGranularity);
	}
	
	/**
	 * build
	 * @throws TException 
	 */
	public MedusaClient build () throws TException {
		
		return new MedusaClient (orionConnectionFactory, medusaTable);
	}
	
	/**
	 * 
	 */
	public void close () {
		
		orionConnectionFactory.close();
	}
		
	/**
	 * manual cleansing 
	 */
	synchronized public void clean (int numItems, int cleanPoll) {
		Date date = new Date ();
	
		try {		
			while (rubber.clean(numItems, date)) Thread.sleep(cleanPoll);
		} catch (InterruptedException e) {
			LOGGER.error("clean error");
		}
	}
	
	/**
	 * start rubber
	 */
    public void startRubber () {
    	rubber.startThread();
    }
    
    /**
     * stop rubber
     */
    public void stopRubber () {
    	rubber.stopThread();
    }	
	
}

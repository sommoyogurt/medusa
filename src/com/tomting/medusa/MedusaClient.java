package com.tomting.medusa;

import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;


import com.tomting.orion.ThrfL2cl;
import com.tomting.orion.ThrfL2cv;
import com.tomting.orion.ThrfL2qb;
import com.tomting.orion.ThrfL2qr;
import com.tomting.orion.ThrfL2st;
import com.tomting.orion.ThrfL2ks;
import com.tomting.orion.ThrfLkey;
import com.tomting.orion.iEcolumntype;
import com.tomting.orion.iEquerytype;
import com.tomting.orion.iEstatetype;
import com.tomting.orion.connection.Helpers;
import com.tomting.orion.connection.OCFI;
import com.tomting.orion.connection.OCI;
import com.tomting.orion.connection.OrionConnection;

public class MedusaClient {
	private final static Logger LOGGER = Logger.getLogger(MedusaClient .class.getName());	
	
	OCFI orionConnectionFactory;
	String medusaTable;
	
	public MedusaClient (OCFI connectionFactory, String table) throws TException {
		
		orionConnectionFactory = connectionFactory;
		medusaTable = table;
	}
	
	/**
	 * release resources
	 */
	public void shutdown () throws TException {	
		
	}
	
	/**
	 * set method
	 * @throws Exception 
	 */
	public boolean set (String key, int timeout, Serializable value) throws Exception {
		String serializedValue = MedusaHelpers.toString(value);
		boolean result = false;
		
		if (key == null || value == null) {
			LOGGER.fatal("null input");
			throw new Exception ();
		}
		ThrfL2st statement = OrionConnection.getStatement();
		statement.cVkey.sVmain = key;
		statement.cVkey.iVstate = iEstatetype.UPSERT;
		statement.cVmutable.sVtable = medusaTable;
        
		ThrfL2cl cVcolumn = new ThrfL2cl ();
        cVcolumn.cVvalue = new ThrfL2cv ();
        cVcolumn.cVvalue.iVtype = iEcolumntype.STRINGTYPE;
        cVcolumn.cVvalue.sVvalue = serializedValue;
        cVcolumn.sVcolumn = Constants.VALUE; 
        statement.cVcolumns.add(cVcolumn);  
        
        Date expireTime = MedusaHelpers.add(new Date (), timeout);        
        cVcolumn = new ThrfL2cl ();
        cVcolumn.cVvalue = new ThrfL2cv ();
        cVcolumn.cVvalue.iVtype = iEcolumntype.STRINGTYPE;
        cVcolumn.cVvalue.sVvalue = Helpers.dateToString(expireTime); 
        cVcolumn.sVcolumn = Constants.TIMESTAMP; 
        statement.cVcolumns.add(cVcolumn);   
        
        OCI orion = orionConnectionFactory.checkOut();        
        try {
        	result = orion.runStatement(statement);        	
		} catch (TException e) {		
			e.printStackTrace();	
		} finally {
			orionConnectionFactory.checkIn(orion);
		}   
        return result;
	}
	
	/**
	 * set method
	 * @throws Exception 
	 */
	public void delete (String key) throws Exception {
	
		if (key == null) {
			LOGGER.fatal("null input");
			throw new Exception ();
		}		
		ThrfL2st statement = OrionConnection.getStatement();
		statement.cVkey.sVmain = key;
		statement.cVkey.iVstate = iEstatetype.DELTMB;
		statement.cVmutable.sVtable = medusaTable;	
		
        OCI orion = orionConnectionFactory.checkOut();        
        try {
        	orion.runStatement(statement);        	
		} catch (TException e) {		
		} finally {
			orionConnectionFactory.checkIn(orion);
		}  		
	}
		
	/**
	 * get method
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public <T> T get (String key) throws Exception {	
		boolean returnedKeyslices;
		ThrfL2ks keyslice;
		T result = null;
	
		if (key == null) {
			LOGGER.fatal("null input");
			throw new Exception ();
		}			
		ThrfL2qr query = OrionConnection.getQuery();
		query.cVmutable.sVtable = medusaTable;	
		query.iVquery = iEquerytype.EXACTQUERY;
		query.cVkey_start = new ThrfLkey ();					
		query.cVkey_start.sVmain = key;		
		
        ThrfL2cl cVcolumn = new ThrfL2cl ();
        cVcolumn.sVcolumn = Constants.VALUE;
        query.cVselect.add(cVcolumn);  		
		
        OCI orion = orionConnectionFactory.checkOut();       
        try {
        	ThrfL2qb queryReturn = orion.runQuery (query);
        	returnedKeyslices = queryReturn.bVreturn;
        	if (returnedKeyslices) {
        		keyslice = queryReturn.cKeyslices.get(0);
        		String opaqueValue = keyslice.cVcolumns.get(0).cVvalue.sVvalue;
        		result = (T) MedusaHelpers.fromString(opaqueValue);
        	}
		} catch (TException e) {		
		} finally {
			orionConnectionFactory.checkIn(orion);
		}        
		return result;
	}
	
	/**
	 * set method
	 * @throws Exception 
	 */
	public <T> T getAndTouch (String key, int timeout) throws Exception {
		T result;
		
		result = get (key);	
		set (key, timeout, (Serializable) result);
		return result;
	}		
	
}

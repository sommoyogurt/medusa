package com.tomting.medusa;

public class Constants {
	
	final static String VALUE = "VALUE";
	final static String TIMESTAMP = "TIMESTAMP";	
	public final static String urlProperty = "javax.jdo.option.ConnectionURL";
	
	final static int cleanGranularity = 1000;
	final static int cleanPoll = 1;	
	final static int cleanCluster = 1000;
}

package com.tomting.medusa;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;

public class MedusaHelpers {
	private final static Logger LOGGER = Logger.getLogger(MedusaHelpers.class.getName());	

    /**
     * string to serializable
     */
	public static Object fromString( String s ) throws IOException ,
                                                        ClassNotFoundException {
        byte [] data = Base64Coder.decode( s );
        ObjectInputStream ois = new ObjectInputStream( 
                                        new ByteArrayInputStream(  data ) );
        Object o  = ois.readObject();
        ois.close();
        return o;
    }

    /**
     * serializable to string
     */
    public static String toString( Serializable o ) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject( o );
        oos.close();
        return new String( Base64Coder.encode( baos.toByteArray() ) );
    }	
    
    /**
     * add milliseconds to a Date
     */
    public static Date add(Date date, int amount) {

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MILLISECOND, amount);
        return c.getTime();
    }    
    
    /**
     * get url from a jdo property file
     * @throws IOException 
     */
    @SuppressWarnings("resource")
	public static String getUrl (String propertyFile, String urlProperty) throws IOException {
        Properties props = new Properties();
        InputStream is = null;
        try {
        	File f = new File(propertyFile);
        	is = new FileInputStream (f);
        } catch (Exception e) {}
        if (is == null) is = MedusaHelpers.class.getResourceAsStream(propertyFile);
        props.load (is);
        is.close();
        return props.getProperty(urlProperty);
    }    
    
    /**
     * get a medusa client builder from a property file
     * @throws IOException 
     */
    public static MedusaClientBuilder getMedusaClientBuilder 
    	(String propertyFile, String urlProperty, String table) {
    	
    	try {
    		return new MedusaClientBuilder (getUrl (propertyFile, urlProperty) , table);
    	} catch (Exception e) {
    		LOGGER.fatal("wrong property file: " + propertyFile + "/" + urlProperty);
    		return null;
    	}
    }
	
}

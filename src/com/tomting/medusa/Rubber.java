package com.tomting.medusa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.tomting.orion.ThrfL2cl;
import com.tomting.orion.ThrfL2cv;
import com.tomting.orion.ThrfL2qb;
import com.tomting.orion.ThrfL2qr;
import com.tomting.orion.ThrfL2st;
import com.tomting.orion.ThrfL2wh;
import com.tomting.orion.ThrfL2ks;
import com.tomting.orion.iEcolumntype;
import com.tomting.orion.iEconditiontype;
import com.tomting.orion.iEquerytype;
import com.tomting.orion.iEstatetype;
import com.tomting.orion.connection.Helpers;
import com.tomting.orion.connection.OCFI;
import com.tomting.orion.connection.OCI;
import com.tomting.orion.connection.OrionConnection;

public class Rubber extends Thread{

	private final static Logger LOGGER = Logger.getLogger(Rubber .class.getName());		
	private OCFI orionConnectionFactory;
	private String medusaTable;	
	private boolean threadCycle;
	
	public Rubber (OCFI connectionFactory, String table, int granularity) {
	
		orionConnectionFactory = connectionFactory;
		medusaTable = table;	
	}
	
	/**
	 * delete numItems elements
	 * local select from MEDUSA where timestamp < 'xxxxxNOWxxxx' order by timestamp desc limit 10
	 */
	synchronized public boolean clean (int numItems, Date date) {
		boolean returnedKeyslices;
		List<ThrfL2ks> keyslices;	
		boolean result = false;
		
		ThrfL2qr query = OrionConnection.getQuery();
		query.cVmutable.sVtable = medusaTable;	
		query.iVquery = iEquerytype.RANGEQUERY;		
		ThrfL2cl cVcondition = new ThrfL2cl ();
		cVcondition.iVconditiontype = iEconditiontype.LECOND;
		cVcondition.sVcolumn = Constants.TIMESTAMP;
		cVcondition.cVvalue = new ThrfL2cv ();
		cVcondition.cVvalue.iVtype = iEcolumntype.STRINGTYPE;
		cVcondition.cVvalue.sVvalue = Helpers.dateToString(date); 
		query.cVwhere = new ThrfL2wh ();
		query.cVwhere.cVcondition = new ArrayList<ThrfL2cl> ();
		query.cVwhere.cVcondition.add(cVcondition);
		query.bVdisableteleport = true;
		query.bVonlysecondary = true;
		query.iVcount = numItems;
		
		ThrfL2st statement = OrionConnection.getStatement();
		statement.cVkey.iVstate = iEstatetype.DELTMB;
		statement.cVmutable.sVtable = medusaTable;	
		
        OCI orion = orionConnectionFactory.checkOut();       
        try {
        	ThrfL2qb queryReturn = orion.runQuery (query);
        	returnedKeyslices = queryReturn.bVreturn;
        	if (returnedKeyslices) {
        		keyslices = queryReturn.cKeyslices;
        		for (int iV = 0; iV < keyslices.size(); iV++) {
        			statement.cVkey.sVmain = keyslices.get(iV).cVkey.sVmain;
        			orion.runStatement(statement);
        			result = true;
        		}
        	}
		} catch (TException e) {	
			LOGGER.fatal("Clean: " + date);
		} finally {
			orionConnectionFactory.checkIn(orion);
		}    	
		return result;
	}
	
    public void run() {
    	boolean empty = true;
    	Date date = null;
    	
    	while (threadCycle) {
    		if (empty) date = new Date ();
    		empty = !clean (Constants.cleanCluster, date);
    		try {
				sleep (empty ? Constants.cleanGranularity : Constants.cleanPoll);
			} catch (InterruptedException e) {}
 
    	}
    }
    
    public void startThread () {
    	threadCycle = true;
    	start ();
    }
    
    public void stopThread () {
    	threadCycle = false;
    }
        
    
}

package debug;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Debug {

	private static final Pattern URL = Pattern.compile("orion:((\\[(\\d+),(\\d+)\\])*)(\\w+):(\\d+)((,*)(\\d+)*):(\\w+)");		
	public static void main(String[] args) {
		//String url = "orion[13,22]:localhost:9001,9002:DEFAULT";
		String url = "orion:[29,30]localhost:9001,9002:DEFAULT";
		
    	Matcher hostMatcher = URL.matcher(url);
		if (!hostMatcher.matches()) throw new UnsupportedOperationException("orion:host:port:namespace");  
		String host = hostMatcher.group(5);		
        int portnio = Integer.parseInt (hostMatcher.group(6));
        String namespace = hostMatcher.group(10);
        int portio = hostMatcher.group(9) != null ? Integer.parseInt (hostMatcher.group(9)) : 0;	
        int workers = hostMatcher.group(3) != null ? Integer.parseInt (hostMatcher.group(3)) : 0;	
        int bulkSize = hostMatcher.group(4) != null ? Integer.parseInt (hostMatcher.group(4)) : 0;	
        System.out.println (host + "-" + portnio + "-" +portio+ "-" + namespace + "-" + workers + "-" + bulkSize);
	}

}

package debug;

import org.apache.log4j.BasicConfigurator;

import com.tomting.medusa.MedusaClient;
import com.tomting.medusa.MedusaClientBuilder;

public class SimpleExample {

	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		BasicConfigurator.configure();
		
		MedusaClientBuilder builder = 
				new MedusaClientBuilder ("orion:[7,50]localhost:9001,9000:DEFAULT", "MEDUSA");
		MedusaClient medusaClient = builder.build ();
		medusaClient.set("hello", 0, "Hello,medusa");
		System.out.println("hello=" + medusaClient.get("hello"));		

		medusaClient.delete("hello");
		System.out.println("hello=" + medusaClient.get("hello"));	
		
		medusaClient.set("hello2", 500, "to check");
		Thread.sleep(200);

		System.out.println("hello2=" + medusaClient.get("hello2"));		
		
		Thread.sleep(1500);		
		
		System.out.println("hello2=" + medusaClient.get("hello2"));	
		
		medusaClient.shutdown();
		builder.close ();
		
		System.out.println ("shutdown");		
	}
}

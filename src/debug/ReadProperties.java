package debug;

import java.io.IOException;

import org.apache.log4j.BasicConfigurator;

import com.tomting.medusa.Constants;
import com.tomting.medusa.MedusaHelpers;

public class ReadProperties {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		BasicConfigurator.configure();
				
		MedusaHelpers.getMedusaClientBuilder ("transactions-optional", Constants.urlProperty, "MEDUSA");
	}

}
